module SalesHelper

  def amount
    ActionController::Base.helpers.number_to_currency(self[:amount])
  end

  def tax
    ActionController::Base.helpers.number_to_currency(self[:tax])
  end

end
