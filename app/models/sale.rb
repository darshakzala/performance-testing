class Sale < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper
  belongs_to :customer

  def amount
    "$%.2f" % self[:amount]
  end

  def tax
    "$%.2f" % self[:tax]
  end

  def amount_with_tax
    self.amount + self.tax
  end


end
